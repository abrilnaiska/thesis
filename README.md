Undergraduate Research

A Natural Language Processing (NLP) research that aims to create a contrastive 
Twitter Opinion Summarizer by using the C-F algorithm that chooses contents of
text summaries based on high contrastiveness and representativeness of English 
tweets. 

The programming language used was Python. It also utilized various packages used
in Data Science like NLTK, numpy, pandas, etc. Aside from that,popular NLP
techniques in cleaning and manipulating the text.

Experiments involved quantitative and qualitative evaluations which showed that
the prototype gained positive results.

This can be applied in business and government intelligence as it is able to 
download and analyze tweets according to a topic. It then generates positive and
negative summaries. Recommendations were done to further improve the research.
