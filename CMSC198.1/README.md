

Research Proposal for CMSC 198.1 by April Joy Padrigano
4th Year 1st Sem, SY 2017-2018

OVERVIEW
The research will focus in one branch of Data Science called Opinion 
Summarization that tries to get an overview of a particular body of text.
We further narrow down the research in exploring a new subfield called 
Contrastive Summarization that creates summaries from different sentiments, in 
this case a positive and negative summary.
This will be the first research that will delve deeper into applying Contrastive
Summarization in microblogs such as Twitter.
